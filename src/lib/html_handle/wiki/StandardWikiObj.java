package lib.html_handle.wiki;

import java.util.Map;

import lib.MyLib;

public class StandardWikiObj {

    private class Query {
        public Map<String, WikiPage> pages;
    }

    @SuppressWarnings("unused")
    private class WikiPage {
        public String pageId;
        public String ns;
        public String title;
    }

    @SuppressWarnings("unused")
    private String batchcomplete;
    private Query query;


    public boolean isPageExist() {
        Map<String, WikiPage> allPages = query.pages;
        return !allPages.containsKey("-1");
    }

}
