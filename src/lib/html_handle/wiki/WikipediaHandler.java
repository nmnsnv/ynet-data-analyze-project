package lib.html_handle.wiki;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.GsonBuilder;

public class WikipediaHandler {

    private static final String REQUEST_FORM = "https://he.wikipedia.org/w/api.php?action=query&format=json&titles=";
    private static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36";

    private static Map<String, Boolean> searchedExpressionsCache = new HashMap<>();


    // NOTE that we can use spaces in out expression since we parse them internally
    public static boolean isExpressionKnown(String expression) throws IOException {
        String wikipediaFitExpression = fitExpressionToWiki(expression);

        if (searchedExpressionsCache.containsKey(wikipediaFitExpression)) {
            return searchedExpressionsCache.get(wikipediaFitExpression);
        }

        String url = REQUEST_FORM + wikipediaFitExpression;
        URL urlObj = new URL(url);
        HttpURLConnection urlCon = (HttpURLConnection) urlObj.openConnection();

        urlCon.setRequestMethod("GET");
        urlCon.setRequestProperty("User-Agent", USER_AGENT);

        BufferedReader in = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
        StandardWikiObj wikiObj = new GsonBuilder().create().fromJson(in, StandardWikiObj.class);
        in.close();

        boolean isPageExist = wikiObj.isPageExist();
        searchedExpressionsCache.put(wikipediaFitExpression, isPageExist);
        return isPageExist;
    }


    private static String fitExpressionToWiki(String expression) {
        return expression.replace(' ', '_');
    }

}
