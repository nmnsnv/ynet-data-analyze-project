package lib.html_handle;

import java.io.File;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import lib.article.Article;
import lib.article.YnetArticle;

public class MyHTMLParser {

    private String filePath;
    private File input;


    public MyHTMLParser(String filePath) {
        this.input = new File(this.filePath);
    }


    public MyHTMLParser(File input) {
        this.input = input;
    }


    public Article parse() throws IOException {
        Document doc = Jsoup.parse(input, "UTF-8");
        String fileName = input.getName();
        return YnetArticle.generateArticle(doc, fileName);
    }

}
