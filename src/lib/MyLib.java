package lib;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import lib.article.Article;

public class MyLib {

    public static <T, V> List<T> map(Collection<V> list, Func<T, V> func) {
        List<T> lst = new ArrayList<>();
        for (V obj : list) {
            lst.add(func.run(obj));
        }
        return lst;
    }


    public static <T> List<T> filter(List<T> list, Func<Boolean, T> func) {
        List<T> lst = new ArrayList<>();
        for (T obj : list) {
            if (func.run(obj)) {
                lst.add(obj);
            }
        }
        return lst;
    }


    public static <T> T getItemFromSet(Set<T> set) {
        for (T obj : set) {
            return obj;
        }
        return null;
    }


    public static Writer openNewFileWriter(String dumpPath) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(dumpPath, "UTF-8");
        return writer;
    }

}
