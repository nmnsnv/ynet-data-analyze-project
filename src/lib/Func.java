package lib;

public interface Func<RETURN_TYPE, RECIEVE_TYPE> {

    public RETURN_TYPE run(RECIEVE_TYPE obj);

}
