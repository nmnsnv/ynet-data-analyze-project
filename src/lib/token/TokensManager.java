package lib.token;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lib.Func;
import lib.MyLib;
import lib.analyzing.WordsCounter;
import lib.article.Article;

// TokensManager responsible for handling tokens in multiple articles
public class TokensManager {

    // saves words and occurences in articles
    // NOTE: if well want to save this for later use we shall use article id rather
    // than an article object
    private Map<String, Map<Article, Integer>> allTokens = new HashMap<>();

    /*
     * TODO can be optimized, rather than delete every time a word is added, we can
     * adjust although for now it'll work fine
     */
    private Map<String, Integer> tokensTotalOccurenceCache = null;


    public TokensManager(List<Article> allArticles, Func<WordsCounter, Article> wordsCounterReciever) {
        addAllArticles(allArticles, wordsCounterReciever);
    }


    public TokensManager() {

    }


    public void addAllArticles(List<Article> allArticles, Func<WordsCounter, Article> wordsCounterReciever) {
        for (Article article : allArticles) {
            addArticle(article, wordsCounterReciever.run(article));
        }
    }


    public void addArticle(Article article, WordsCounter allArticleWords) {
        // WordsCounter allArticleWords = article.getWordsCounter();
        for (String word : allArticleWords.getAllWords()) {
            putWord(word, article);
        }
    }


    private void putWord(String word, Article article) {

        // since cache no longer relevent, delete it
        tokensTotalOccurenceCache = null;

        Map<Article, Integer> wordRefs;
        Integer occurenceInArticle = article.getWordsCounter().getTF(word);

        if (allTokens.containsKey(word)) {
            wordRefs = allTokens.get(word);
        } else {
            wordRefs = new HashMap<>();
            allTokens.put(word, wordRefs);
        }

        wordRefs.put(article, occurenceInArticle);
    }


    public Map<String, Map<Article, Integer>> getAllTokens() {
        return this.allTokens;
    }


    public Map<String, Integer> getTokensArticleFreq() {

        if (tokensTotalOccurenceCache != null) {
            return this.tokensTotalOccurenceCache;
        }

        Map<String, Integer> tokensOccurence = new HashMap<>();
        for (String word : this.allTokens.keySet()) {
            Map<Article, Integer> occureIn = this.allTokens.get(word);
            int totalOccurences = occureIn.size();
            tokensOccurence.put(word, totalOccurences);
        }
        tokensTotalOccurenceCache = tokensOccurence;
        return tokensOccurence;
    }

}
