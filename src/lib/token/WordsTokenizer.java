package lib.token;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import lib.MyLib;
import lib.article.Article;

// This handles all the article words and translates them for tokens
public class WordsTokenizer {

    // public String rawContent;
    private List<String> words;
    private String content = "";

    private String unwantedCharsStart = "\"\'";
    private String unwantedCharsEnd = "\"";


    public WordsTokenizer(String content) {
        this.content = content;
    }


    public WordsTokenizer() {
    }


    public void addContent(String addedContent) {
        this.content += " " + addedContent;
    }


    public void tokenize() {
        String hebrewChars = "א-ת";
        String[] words = content.split("([^a-zA-Z" + hebrewChars + "\"']+)'*\\1*");

        /*
         * TODO improve this method, although not very critical since its wont cause
         * relatively heavy performence issues Validate the words, should not be very
         * costly, this is done to fix the regex mistakes, its works well enough to be
         * implemented this way for now
         */
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            boolean keepCleaning = true;
            while (keepCleaning) {
                keepCleaning = false;
                if (word.length() == 0 || word.isEmpty()) {
                    words[i] = null;
                    break;
                }
                if (unwantedCharsStart.indexOf(word.charAt(0)) >= 0) {
                    word = word.substring(1);
                    words[i] = word;
                    keepCleaning = true;
                }
                if (word.length() == 0) {
                    continue;
                }
                int wordLastIdx = word.length() - 1;
                if (unwantedCharsEnd.indexOf(word.charAt(wordLastIdx)) >= 0) {
                    word = word.substring(0, wordLastIdx);
                    words[i] = word;
                    keepCleaning = true;
                }
            }
        }

        List<String> wordsList = Arrays.asList(words);
        List<String> cleanedWordsList = MyLib.filter(wordsList, (obj) -> obj != null);
        if (cleanedWordsList.toString().contains(", ,")) {
            System.out.println("XXXX");
            System.out.println(cleanedWordsList);
        }
        this.words = cleanedWordsList;
    }


    // Note that tokens can occur more than once in here!
    public List<String> getAllTokens() {
        return this.words;
    }

}
