package lib.article;

import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import lib.analyzing.WordsCounter;
import lib.token.WordsTokenizer;

// YnetArticle knows only how to handle a ynet article Document, and parses it by itself.

public class YnetArticle {

    private String content;
    private String title;

    // NOTE: Very Important! the id must be unique, we use it as the file name and
    // thus all the ynet files should be under same directory to avoid collisions
    private String id;
    private String authorName;

    private Map<String, Integer> wordsCount;
    private String date;


    public YnetArticle(Document doc, String id) {
    }


    public static Article generateArticle(Document doc, String id) {
        Article article = new Article();
        cleanDoc(article, doc, id);

        setId(id, article);
        setTitle(doc, article);
        setContent(doc, article);
        setAuthor(doc, article);

        // setDate(doc,article);

        // more advanced setups, requires the basic info!
        setTags(doc, article);
        setSubjects(doc, article);

        // analyzing the info, requires all previous info
        setWordsCount(article);

        return article;
    }


    private static void cleanDoc(Article article, Document doc, String id) {
        Element findErrorDiv = doc.getElementById("find_error");
        if (findErrorDiv == null) {
            setId(id, article);
            setTitle(doc, article);
            return;
        }
        findErrorDiv.remove();
    }


    private static void setSubjects(Document doc, Article article) {
        Elements tagsElem = doc.getElementsByClass("trj_top");
        Element subjSuperTag = tagsElem.get(0);
        // Element subjectsTag = subjSuperTag.getElementsByTag("ul").get(0);

        for (Element subjAllListsTag : subjSuperTag.getElementsByTag("ul")) {

            Elements subjListTags = subjAllListsTag.getElementsByTag("li");
            for (Element elem : subjListTags) {
                // System.out.println(elem.text());
                article.addSubject(elem.text());
            }
        }

    }


    private static void setTags(Document doc, Article article) {
        Element tagsElem = doc.getElementById("articletags");
        if (tagsElem == null) {
            return;
        }

        Elements tags = tagsElem.getElementsByTag("a");
        if (tags == null) {
            return;
        }

        for (Element tag : tags) {
            article.addTag(tag.text());
        }
    }


    // TODO: make date special obj, not string, for now this does not work very well
    // at parsing the date.
    private static void setDate(Document doc, Article article) {
        Element headerClass = doc.getElementsByClass("art_header_footer").get(0);
        Element authorElement = headerClass.getElementsByClass("art_header_footer_author").get(1);
        String dateStr = retrieveAllText(authorElement);
    }


    private static void setAuthor(Document doc, Article article) {
        Element headerClass = doc.getElementsByClass("art_header_footer").get(0);
        Element authorElement = headerClass.getElementsByClass("art_header_footer_author").get(0);
        String authorName = retrieveAllText(authorElement);
        article.setAuthorName(authorName);
    }


    private static void setWordsCount(Article article) {
        article.generateWordsCounter();
    }


    private static void setId(String id, Article article) {
        article.setId(id);
    }


    // TODO: improve a little, this still returns some irrelevant data *NOT
    // CRITICAL*
    private static String retrieveAllText(Element fromElement) {
        // We convert the element to document so we could extract all the content at
        // once

        String elemString = fromElement.toString();
        Document elemDoc = Jsoup.parse(elemString);
        String body = elemDoc.body().text();
        return body;
    }


    private static void setTitle(Document doc, Article article) {
        article.setTitle(doc.title());
    }


    private static void setContent(Document doc, Article article) {
        Element mainBody = doc.getElementsByClass("text14").get(0);
        String content = retrieveAllText(mainBody);
        article.setContent(content);
    }

}
