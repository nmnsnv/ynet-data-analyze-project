package lib.article;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import lib.Func;
import lib.article.author.AuthorsBook;

public class ArticlesBook {
    List<Article> allArticles = new LinkedList<>();
    // loadAllArticles(articlesPath, authorsInfo);


    public ArticlesBook() {

    }


    public List<Article> getAllArticles() {
        return this.allArticles;
    }


    public void loadArticlesInDir(String path, AuthorsBook authorsInfo, Func<Article, File> loadBookMethod)
            throws IOException {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (!file.isFile()) {
                continue;
            }
            Article curArticle = loadBookMethod.run(file);
            addArticle(curArticle);
            authorsInfo.addArticle(curArticle);
        }
    }


    public void addArticle(Article curArticle) {
        allArticles.add(curArticle);
    }


    public int getSize() {
        return this.allArticles.size();
    }


    public Article getArticleByIndex(int i) {
        return this.allArticles.get(i);
    }

}
