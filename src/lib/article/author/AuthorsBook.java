package lib.article.author;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.article.Article;
import lib.token.TokensManager;

// the object of the class responsible for keeping
// track of authors with different names,
// all authors here must have at least 1 article,
// otherwise they wont be able to get inside
public class AuthorsBook {

    private Map<String, ArticleAuthor> allAuthors = new HashMap<>();


    public AuthorsBook() {

    }


    public void addArticle(Article article) {
        String articleAuthorName = article.getAuthorName().toLowerCase();
        ArticleAuthor author;
        if (allAuthors.containsKey(articleAuthorName)) {
            author = allAuthors.get(articleAuthorName);
        } else {
            author = new ArticleAuthor(articleAuthorName);
            allAuthors.put(articleAuthorName, author);
        }

        author.addArticle(article);

    }


    public ArticleAuthor getAuthor(String authorName) {
        return this.allAuthors.get(authorName.toLowerCase());
    }


    public Map<String, ArticleAuthor> getAllAuthors() {
        return this.allAuthors;
    }


    public Map<String, Map<ArticleAuthor, Integer>> sortAuthorsBySubject() {
        Map<String, Map<ArticleAuthor, Integer>> authorsSortBySubject = new HashMap<>();

        for (ArticleAuthor author : allAuthors.values()) {
            TokensManager tm = author.getSubjectsTM();
            Map<String, Integer> authorSubjsOcrs = tm.getTokensArticleFreq();
            for (String subjectName : authorSubjsOcrs.keySet()) {
                Map<ArticleAuthor, Integer> subjectMap;
                if (authorsSortBySubject.containsKey(subjectName)) {
                    subjectMap = authorsSortBySubject.get(subjectName);
                } else {
                    subjectMap = new HashMap<>();
                    authorsSortBySubject.put(subjectName, subjectMap);
                }
                Integer occurence = authorSubjsOcrs.get(subjectName);
                subjectMap.put(author, occurence);
            }
        }
        return authorsSortBySubject;
    }


    /*
     * TODO possible to cache it if needed although is very dangerous! consider
     * cache carefully! NOTE that the authors cache their subjects internally so
     * anyway the call should not be vey costly
     */
    public Set<String> getAllSubjectsInBook() {
        Set<String> allSubjs = new HashSet<>();
        for (ArticleAuthor author : allAuthors.values()) {
            // ArticleAuthor author = allAuthors.get(authorName);
            Set<String> authorSubjs = author.getAuthorSubjects();

            // Note that we dont merge the because if the sets will change in the future we
            // dont want to change them
            for (String subjectName : authorSubjs) {
                allSubjs.add(subjectName);
            }
        }
        return allSubjs;
    }


    // TODO possible to cache it if needed
    public List<Article> getAllArticles() {
        List<Article> allArticles = new LinkedList<>();
        for (ArticleAuthor author : allAuthors.values()) {
            // ArticleAuthor author = allAuthors.get(authorName);
            for (Article article : author.getAllArticles()) {
                allArticles.add(article);
            }
        }
        return allArticles;
    }

}
