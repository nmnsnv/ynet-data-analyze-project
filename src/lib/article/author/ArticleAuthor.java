package lib.article.author;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lib.Pair;
import lib.analyzing.TfIdfCalc;
import lib.analyzing.WordsCounter;
import lib.article.Article;
import lib.token.TokensManager;

// An object represents articles author
public class ArticleAuthor {

    private String name;
    private Set<Article> authorArticles = new HashSet<>();
    private TokensManager tmTitleContent = new TokensManager();
    private TokensManager tmSubjects = new TokensManager();


    public ArticleAuthor(String name) {
        this.name = name;
    }


    public String getName() {
        return this.name;
    }


    public void addArticle(Article article) {
        this.authorArticles.add(article);
        tmTitleContent.addArticle(article, article.getWordsCounter());

        WordsCounter subjectsWC = new WordsCounter();
        subjectsWC.countWords(article.getSubjects());
        tmSubjects.addArticle(article, subjectsWC);
    }


    public TokensManager getSubjectsTM() {
        return this.tmSubjects;
    }


    public Set<Article> getAllArticles() {
        return this.authorArticles;
    }


    /*
     * TODO possible to improve, maybe use combination of tags and subjects for
     * words
     */
    public List<Pair<String, Double>> getImportantWords(TfIdfCalc tfIdfCalc) throws IOException {
        Set<String> allWords = tmTitleContent.getAllTokens().keySet();

        // the string is the word, and it contains all the author articles and its score
        // per article
        Map<String, Map<Article, Double>> tfIdfScores = new HashMap<>();
        for (String word : allWords) {
            calcWordAllTfIdf(word, tfIdfScores, tfIdfCalc);
        }

        List<Pair<String, Double>> importantWordsOrdered = getImportantWordsOrdered(tfIdfScores);

        return importantWordsOrdered;
    }


    private List<Pair<String, Double>> getImportantWordsOrdered(Map<String, Map<Article, Double>> tfIdfScores) {
        List<Pair<String, Double>> wordsHighestScore = getWordsHighestScore(tfIdfScores);

        Collections.sort(wordsHighestScore, new Comparator<Pair<String, Double>>() {
            @Override
            public int compare(Pair<String, Double> c1, Pair<String, Double> c2) {
                // we should ensure that list doesn't contain null values!
                return c1.getObj2().compareTo(c2.getObj2());
            }
        });

        Collections.reverse(wordsHighestScore);

        return wordsHighestScore;
    }


    // returns a map with words and the highest tf-idf score the words recieved in
    // one of the author articles
    private List<Pair<String, Double>> getWordsHighestScore(Map<String, Map<Article, Double>> tfIdfScores) {
        // Important! make sure we dont insert same word twice, we use a list rather
        // than a set since its easier to sort later on, although a set would be safer
        List<Pair<String, Double>> wordsHighestScore = new LinkedList<>();

        for (String word : tfIdfScores.keySet()) {
            // NOTE: -10d is arbitrary, any number below 0 will do just as fine
            double highestScore = -10d;
            Map<Article, Double> wordScores = tfIdfScores.get(word);
            for (Article article : wordScores.keySet()) {
                double curScore = wordScores.get(article);
                if (curScore > highestScore) {
                    highestScore = curScore;
                }
            }
            wordsHighestScore.add(new Pair<>(word, highestScore));
        }

        return wordsHighestScore;
    }


    // calcs all tf-idf scores for a word in author articles
    private Map<Article, Double> calcWordAllTfIdf(String word, Map<String, Map<Article, Double>> tfIdfScores,
            TfIdfCalc tfIdfCalc) throws IOException {
        Map<Article, Double> wordScores;

        // check if word exists in memory, if not create it
        if (tfIdfScores.containsKey(word)) {
            wordScores = tfIdfScores.get(word);
        } else {
            wordScores = new HashMap<>();
            tfIdfScores.put(word, wordScores);
        }

        for (Article curArticle : this.authorArticles) {
            Double tfIdfScore = tfIdfCalc.calcTfIdf(word, curArticle);
            wordScores.put(curArticle, tfIdfScore);
        }

        return wordScores;
    }


    public Map<String, Map<Article, Double>> getAllWordsTfIdfScore(TfIdfCalc tfIdfCalc) throws IOException {
        Set<String> allWords = tmTitleContent.getAllTokens().keySet();

        // the string is the word, and it contains all the author articles and its score
        // per article
        Map<String, Map<Article, Double>> tfIdfScores = new HashMap<>();
        for (String word : allWords) {
            calcWordAllTfIdf(word, tfIdfScores, tfIdfCalc);
        }
        return tfIdfScores;
    }


    public Set<String> getAuthorSubjects() {
        TokensManager authorSubjectsTM = this.getSubjectsTM();
        Set<String> allAuthorSubjects = authorSubjectsTM.getAllTokens().keySet();
        return allAuthorSubjects;
    }

}
