package lib.article;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import lib.analyzing.WordsCounter;
import lib.token.WordsTokenizer;

public class Article {

    private String id;
    private String authorName;
    private String content;
    private String title;

    private List<String> subjects = new LinkedList<>();
    private List<String> tags = new LinkedList<>();

    private WordsCounter wordsCount;


    public Article() {

    }


    public void generateWordsCounter() {
        WordsTokenizer myTokenizer = new WordsTokenizer();
        myTokenizer.addContent(this.getTitle());
        myTokenizer.addContent(this.getContent());

        myTokenizer.tokenize();
        List<String> allTokens = myTokenizer.getAllTokens();
        WordsCounter wc = new WordsCounter();
        wc.countWords(allTokens);
        this.setWordsCounter(wc);
    }


    public int getTF(String token) {
        if (!this.wordsCount.containsKey(token)) {
            return 0;
        } else {
            return this.wordsCount.getTF(token);
        }
    }


    public Set<String> getAllWords() {
        return this.wordsCount.getAllWords();
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getAuthorName() {
        return authorName;
    }


    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public WordsCounter getWordsCounter() {
        return wordsCount;
    }


    public void setWordsCounter(WordsCounter wordsCounter) {
        this.wordsCount = wordsCounter;
    }


    public List<String> getSubjects() {
        return subjects;
    }


    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }


    public void addSubject(String subject) {
        this.subjects.add(subject);
    }


    public List<String> getTags() {
        return tags;
    }


    public void setTags(List<String> tags) {
        this.tags = tags;
    }


    public void addTag(String tag) {
        this.tags.add(tag);
    }

}
