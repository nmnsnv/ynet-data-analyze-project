package lib.analyzing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WordsCounter {

    private Map<String, Integer> wordsCount = new HashMap<>();


    public WordsCounter() {

    }


    public void addWord(String word) {
        Integer wordOccurence = wordsCount.get(word);

        if (wordOccurence == null) {
            wordOccurence = 1;
        } else {
            wordOccurence = wordOccurence + 1;
        }
        wordsCount.put(word, wordOccurence);
    }


    public void countWords(List<String> allWords) {
        for (String word : allWords) {
            addWord(word);
        }
    }


    public Map<String, Integer> getWordsCounter() {
        return this.wordsCount;
    }


    public boolean containsKey(String token) {
        return this.wordsCount.containsKey(token);
    }


    public int getTF(String token) {
        if (this.wordsCount.containsKey(token)) {
            return this.wordsCount.get(token);
        }
        return 0;
    }


    public Set<String> getAllWords() {
        return this.wordsCount.keySet();
    }

}
