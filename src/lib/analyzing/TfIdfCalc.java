package lib.analyzing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;

import lib.MyLib;
import lib.Pair;
import lib.article.Article;
import lib.article.ArticlesBook;
import lib.html_handle.wiki.WikipediaHandler;

public class TfIdfCalc {

    // public static final String TF_IDF_CALC_DEFAULT_PATH =
    // "/home/noam/work/projects/army/ynet/parsed_data/tfIdfCalcCache.cache";

    // NOTE: the first string represents the word, second is the article id, we save
    // the article id so we could retrieve it from the hard drive later on
    private Map<String, Map<String, Double>> tfIdfCalcCache = new HashMap<>();
    private Map<String, Integer> docFreqCache = new HashMap<>();
    private boolean useWikiFlag;

    private ArticlesBook articlesBook;
    private double wikiKnownCoefficient = 3d;


    public TfIdfCalc(ArticlesBook allArticles, boolean useWikiFlag) {
        this.useWikiFlag = useWikiFlag;
        this.articlesBook = allArticles;
    }


    public TfIdfCalc(boolean useWikiFlag) {
        this.useWikiFlag = useWikiFlag;
        this.articlesBook = new ArticlesBook();
    }


    public void setWikiKnownCoefficient(double newCoefficient) {
        this.wikiKnownCoefficient = newCoefficient;
    }


    /*
     * NOTE: usage is NOT RECOMMENDED at all since this makes all the cache to be
     * deleted, and is very performance costly
     * 
     * Another NOTE is that it is probably cant be solved since the tfidf calc
     * depends on a complete set of articles and even slight change in the articles
     * set can change the tfidf calc entirely and thus making the cache totaly
     * worthless
     */
    public void addArticle(Article article) {
        this.clearCache();
        this.articlesBook.addArticle(article);
    }


    public void dumpTfIdfCalcCache(String filePath) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"));
        JsonWriter jsonWriter = new JsonWriter(writer);

        Type type = new TypeToken<Map<String, Map<String, Double>>>() {
        }.getType();

        new GsonBuilder().create().toJson(tfIdfCalcCache, type, jsonWriter);
        writer.close();
    }


    public void loadTfIdfCalcCache(String cacheFilePath) throws IOException {
        try {
            BufferedReader br = new BufferedReader(new FileReader(cacheFilePath));
            Type type = new TypeToken<Map<String, Map<String, Double>>>() {
            }.getType();

            this.tfIdfCalcCache = new GsonBuilder().create().fromJson(br, type);
            System.out.println("tfIdfCalcCache size: " + tfIdfCalcCache.size());
        } catch (FileNotFoundException e) {
            System.out.println("warn :: could not find TfIdfCalc cache file");
            return;
        }
    }


    public void clearCache() {
        this.docFreqCache = new HashMap<>();
        this.tfIdfCalcCache = new HashMap<>();
    }


    public List<String> getDocumentSortedWordsTfIdf(Article article) throws IOException {
        List<Pair<String, Double>> sortedWordsTfPairs = new LinkedList<>();
        for (String word : article.getAllWords()) {
            sortedWordsTfPairs.add(new Pair<String, Double>(word, calcTfIdf(word, article)));
        }

        Collections.sort(sortedWordsTfPairs, new Comparator<Pair<String, Double>>() {
            @Override
            public int compare(Pair<String, Double> c1, Pair<String, Double> c2) {
                // You should ensure that list doesn't contain null values!
                return c1.getObj2().compareTo(c2.getObj2());
            }
        });

        Collections.reverse(sortedWordsTfPairs);

        return MyLib.map(sortedWordsTfPairs, (pair) -> pair.getObj1());
    }


    public double calcTfIdf(String token, Article article) throws IOException {
        if (isTfIdfCached(token, article)) {
            return getCachedTfIdfCalc(token, article);
        }
        double tfIdfVal = getTf(token, article) * getIdf(token);
        if (useWikiFlag) {
            boolean isTokenKnown = WikipediaHandler.isExpressionKnown(token);
            if (isTokenKnown) {
                tfIdfVal *= this.wikiKnownCoefficient;
            }
        }
        cacheTfIdfCalc(token, article, tfIdfVal);
        return tfIdfVal;
    }


    private void cacheTfIdfCalc(String token, Article article, double tfIdfVal) {
        Map<String, Double> cachedKeys;
        if (tfIdfCalcCache.containsKey(token)) {
            cachedKeys = tfIdfCalcCache.get(token);
        } else {
            cachedKeys = new HashMap<>();
            tfIdfCalcCache.put(token, cachedKeys);
        }

        cachedKeys.put(article.getId(), tfIdfVal);
    }


    private double getCachedTfIdfCalc(String token, Article article) {
        // NOTE: this should be called only after we validate that there's a cache by
        // calling isCached otherwise we might crash
        return tfIdfCalcCache.get(token).get(article.getId());
    }


    private boolean isTfIdfCached(String token, Article article) {
        if (tfIdfCalcCache.containsKey(token)) {
            Map<String, Double> cachedArticles = tfIdfCalcCache.get(token);
            if (cachedArticles.containsKey(article.getId())) {
                return true;
            }
        }
        return false;
    }


    public double getTf(String token, Article article) {
        return article.getTF(token);
    }


    public double getIdf(String token) {
        int docFreq = getDocFreq(token);
        if (docFreq == 0) {
            return 0;
        }
        double idf = Math.log(articlesBook.getSize() / (docFreq));
        return idf;
    }


    public int getDocFreq(String token) {
        if (isCachedDocFreq(token)) {
            return getCachedDocFreq(token);
        }

        int docFreq = 0;
        for (Article article : articlesBook.getAllArticles()) {
            if (article.getTF(token) > 0) {
                docFreq++;
            }
        }

        cacheDocFreq(token, docFreq);
        return docFreq;
    }


    private boolean isCachedDocFreq(String token) {
        return docFreqCache.containsKey(token);
    }


    private void cacheDocFreq(String token, int docFreq) {
        docFreqCache.put(token, docFreq);
    }


    private int getCachedDocFreq(String token) {
        // NOTE: use this only after checked if the value is cached! otherwise risk
        // program crash!
        return docFreqCache.get(token);
    }

}
