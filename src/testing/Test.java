package testing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public abstract class Test {

    public abstract void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException;

}
