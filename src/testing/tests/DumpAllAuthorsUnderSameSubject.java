package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.GsonBuilder;

import lib.MyLib;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class DumpAllAuthorsUnderSameSubject extends Test {

    private String dumpPath;
    private AuthorsBook authorsInfo;

    class AuthorWithSubj {
        public String authorName;
        public int articlesWithSubject;
    }

    class SubjectFormattedObj {
        public String subject;
        public List<AuthorWithSubj> allAuthorsWithSubject = new LinkedList<>();
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        List<SubjectFormattedObj> allSubjs = new LinkedList<>();

        Map<String, Map<ArticleAuthor, Integer>> sortedSubjs = authorsInfo.sortAuthorsBySubject();
        for (String subj : sortedSubjs.keySet()) {
            Map<ArticleAuthor, Integer> sharingSubjAuthors = sortedSubjs.get(subj);
            SubjectFormattedObj sfo = new SubjectFormattedObj();
            allSubjs.add(sfo);

            sfo.subject = subj;

            for (ArticleAuthor author : sharingSubjAuthors.keySet()) {
                Integer articlesWithSubject = sharingSubjAuthors.get(author);

                AuthorWithSubj aws = new AuthorWithSubj();
                sfo.allAuthorsWithSubject.add(aws);

                aws.authorName = author.getName();
                aws.articlesWithSubject = articlesWithSubject;
            }
        }

        Writer writer = MyLib.openNewFileWriter(dumpPath);
        new GsonBuilder().setPrettyPrinting().create().toJson(allSubjs, writer);
    }


    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }

}
