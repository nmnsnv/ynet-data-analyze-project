package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import lib.Pair;
import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class AuthorWordImportanceInArticlesTest extends Test {

    private AuthorsBook authorsInfo;
    private TfIdfCalc tfIdfCalc;
    private String authorName;


    public AuthorWordImportanceInArticlesTest(AuthorsBook authorsInfo, TfIdfCalc tfIdfCalc) {
        this.authorsInfo = authorsInfo;
        this.tfIdfCalc = tfIdfCalc;
    }


    public AuthorWordImportanceInArticlesTest() {
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        ArticleAuthor author = authorsInfo.getAuthor(authorName);
        System.out.println("HERE! author? " + author);
        Map<String, Map<Article, Double>> allWordsTfIdfScore = author.getAllWordsTfIdfScore(tfIdfCalc);

        System.out.println("Start searching page");
        @SuppressWarnings("resource")
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("enter word");
            String wordToSearch = in.nextLine();
            List<Pair<Article, Double>> wordsScore = new LinkedList<>();
            Map<Article, Double> wordScores = allWordsTfIdfScore.get(wordToSearch);

            if (wordScores == null) {
                System.out.println("NO MATCH FOUND!");
                continue;
            }

            for (Article article : wordScores.keySet()) {
                // writer.println("for word: " + word + " tf-idf = " + score);
                Pair<Article, Double> articleScorePair = new Pair<Article, Double>(article, wordScores.get(article));
                // Pair<String, Pair<Article, Double>> scoreObj = new Pair<>(word,
                // articleScorePair);
                wordsScore.add(articleScorePair);
            }
            // Map<String, Double> x =
            Collections.sort(wordsScore, new Comparator<Pair<Article, Double>>() {
                @Override
                public int compare(Pair<Article, Double> c1, Pair<Article, Double> c2) {
                    // You should ensure that list doesn't contain null values!
                    return c1.getObj2().compareTo(c2.getObj2());
                }
            });

            Collections.reverse(wordsScore);

            for (Pair<Article, Double> pair : wordsScore) {
                Article article = pair.getObj1();
                String articleName = article.getTitle();
                String articleId = article.getId();
                Double score = pair.getObj2();
                System.out.println(
                        "for word: " + wordToSearch + " tf-idf = " + score + " tf:" + article.getTF(wordToSearch)
                        // + " idf:"
                                + " || article title: " + articleName + " || article id: " + articleId);
            }
        }
    }
    
    
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }


    public AuthorsBook getAuthorsInfo() {
        return authorsInfo;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }

}
