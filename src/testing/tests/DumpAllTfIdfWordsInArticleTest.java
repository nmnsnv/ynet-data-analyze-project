package testing.tests;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import lib.Pair;
import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import testing.Test;

public class DumpAllTfIdfWordsInArticleTest extends Test {

    private Article article;
    private TfIdfCalc tfIdfCalc;
    private String dumpPath;


    public DumpAllTfIdfWordsInArticleTest(Article article, TfIdfCalc tfIdfCalc, String dumpPath) {
        this.article = article;
        this.tfIdfCalc = tfIdfCalc;
        this.dumpPath = dumpPath;
    }


    public DumpAllTfIdfWordsInArticleTest() {
    }


    public Article getArticle() {
        return article;
    }


    public void setArticle(Article article) {
        this.article = article;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public String getDumpPath() {
        return dumpPath;
    }


    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }


    @Override
    public void runTest() throws IOException {
        System.out.println("Start searching page");
        PrintWriter writer = new PrintWriter(dumpPath, "UTF-8");
        writer.println("PATH: " + article.getId());
        writer.println("TITLE: " + article.getTitle());
        List<Pair<String, Double>> wordsScore = new LinkedList<>();
        for (String word : article.getWordsCounter().getAllWords()) {
            Double score = tfIdfCalc.calcTfIdf(word, article);
            // writer.println("for word: " + word + " tf-idf = " + score);
            wordsScore.add(new Pair<String, Double>(word, score));
        }
        // Map<String, Double> x =
        Collections.sort(wordsScore, new Comparator<Pair<String, Double>>() {
            @Override
            public int compare(Pair<String, Double> c1, Pair<String, Double> c2) {
                // should ensure that list doesn't contain null values!
                return c1.getObj2().compareTo(c2.getObj2());
            }
        });

        for (Pair<String, Double> pair : wordsScore) {
            String word = pair.getObj1();
            Double score = pair.getObj2();
            writer.println("for word: " + word + " tf-idf = " + score);

        }

        writer.close();
    }
}
