package testing.tests;

import java.io.IOException;
import java.util.Scanner;

import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.ArticlesBook;
import testing.Test;

public class WordScannerTest extends Test {

    private ArticlesBook allArticles;
    private TfIdfCalc tfIdfCalc;
    private Double minTfIdf = -1d;


    public WordScannerTest(ArticlesBook allArticles, TfIdfCalc tfIdfCalc) {
        this.allArticles = allArticles;
        this.tfIdfCalc = tfIdfCalc;
    }


    public WordScannerTest() {
    }


    @Override
    public void runTest() throws IOException {
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("enter term:");
            String term = scanner.nextLine();
            System.out.println("Your term is |" + term + "|");
            // String term = "ישראל";
            double idf = tfIdfCalc.getIdf(term);
            int lowScoreCount = 0;
            for (int i = 0; i < allArticles.getSize(); i++) {
                Article curArticle = allArticles.getArticleByIndex(i);
                double tf = tfIdfCalc.getTf(term, curArticle);

                double tfIdf = tfIdfCalc.calcTfIdf(term, curArticle);
                if (tfIdf >= minTfIdf) {
                    System.out.println("term: " + term);
                    System.out.println(curArticle.getId());
                    System.out.println(curArticle.getTitle());
                    System.out.println("doc" + i);
                    System.out.println("tf: " + tf);
                    System.out.println("idf: " + idf);
                    System.out.println("tf-idf: " + tfIdf);
                    System.out.println("\n");
                } else {
                    lowScoreCount++;
                }
            }
            System.out.println("There are " + lowScoreCount + " articles with less than 1 tf-idf");
        }
    }


    public ArticlesBook getAllArticles() {
        return allArticles;
    }


    public void setMinTfIdf(double minTfIdfScore) {
        this.minTfIdf = minTfIdfScore;
    }


    public void setAllArticles(ArticlesBook allArticles) {
        this.allArticles = allArticles;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }

}
