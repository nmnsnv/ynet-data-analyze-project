package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;

import lib.analyzing.TfIdfCalc;
import lib.article.ArticlesBook;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class FindAuthorsForAuthorWithSubj extends Test {

    private TfIdfCalc tfIdfCalc;
    private ArticlesBook allArticles;
    private AuthorsBook authorsInfo;
    private String authorName;


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        Map<String, Map<ArticleAuthor, Integer>> sortedSubjs = authorsInfo.sortAuthorsBySubject();

        ArticleAuthor author = authorsInfo.getAuthor(authorName);

        Set<String> authorSubjs = author.getAuthorSubjects();

        System.out.println("all author " + authorName + " subjects:");

        for (String subj : authorSubjs) {
            Map<ArticleAuthor, Integer> sharingSubjAuthors = sortedSubjs.get(subj);

            System.out.println("users share author subj " + subj + " amount: " + (sharingSubjAuthors.size() - 1));

            for (ArticleAuthor curAuthor : sharingSubjAuthors.keySet()) {
                if (curAuthor == author) {
                    continue;
                }

                Integer articlesWithSubject = sharingSubjAuthors.get(curAuthor);
                System.out.println("author: " + curAuthor.getName() + " has " + articlesWithSubject + " out of "
                        + curAuthor.getAllArticles().size() + " articles");
            }
            System.out.println();
        }

    }


    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public void setAllArticles(ArticlesBook allArticles) {
        this.allArticles = allArticles;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }

}
