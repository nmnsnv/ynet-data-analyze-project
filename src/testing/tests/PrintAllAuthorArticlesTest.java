package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class PrintAllAuthorArticlesTest extends Test {

    private TfIdfCalc tfIdfCalc;
    private String authorName;
    private AuthorsBook authorsInfo;


    public PrintAllAuthorArticlesTest() {
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        ArticleAuthor author = authorsInfo.getAuthor(authorName);
        System.out.println("Author " + authorName);
        for (Article article : author.getAllArticles()) {
            System.out.println(
                    "id: " + article.getId() + " title: " + article.getTitle() + " subj: " + article.getSubjects());
        }
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public void setAuthor(String authorName) {
        this.authorName = authorName;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }

}
