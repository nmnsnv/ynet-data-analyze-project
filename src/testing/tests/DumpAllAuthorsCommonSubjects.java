package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.GsonBuilder;

import lib.MyLib;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class DumpAllAuthorsCommonSubjects extends Test {

    private String dumpPath;
    private AuthorsBook authorsInfo;

    class AuthorCompanion {
        public String authorName;
        public int overlappingSubjects;
        public int authorTotalArticles;

    }

    class AuthorPossibleCompanions {

        public String authorName;
        public int totalArticles;
        public Set<String> authorSubjs;
        public List<AuthorCompanion> authorsCompanions = new LinkedList<>();
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        // ArticleAuthor author = authorsInfo.getAuthor(authorName);
        List<AuthorPossibleCompanions> allAuthorsPossibleCompanions = new LinkedList<>();

        Map<String, Map<ArticleAuthor, Integer>> sortedSubjs = authorsInfo.sortAuthorsBySubject();
        for (ArticleAuthor author : authorsInfo.getAllAuthors().values()) {
            Set<String> authorSubjs = author.getAuthorSubjects();

            AuthorPossibleCompanions authorPosComp = new AuthorPossibleCompanions();
            allAuthorsPossibleCompanions.add(authorPosComp);

            authorPosComp.authorName = author.getName();
            authorPosComp.totalArticles = author.getAllArticles().size();
            authorPosComp.authorSubjs = authorSubjs;

            for (String subj : authorSubjs) {
                Map<ArticleAuthor, Integer> sharingSubjAuthors = sortedSubjs.get(subj);

                for (ArticleAuthor curAuthor : sharingSubjAuthors.keySet()) {
                    if (curAuthor == author) {
                        continue;
                    }

                    Integer articlesWithSubject = sharingSubjAuthors.get(curAuthor);
                    AuthorCompanion ac = new AuthorCompanion();
                    authorPosComp.authorsCompanions.add(ac);

                    ac.authorName = curAuthor.getName();
                    ac.overlappingSubjects = articlesWithSubject;
                    ac.authorTotalArticles = curAuthor.getAllArticles().size();
                }
            }
        }

        Writer writer = MyLib.openNewFileWriter(dumpPath);
        new GsonBuilder().setPrettyPrinting().create().toJson(allAuthorsPossibleCompanions, writer);

    }


    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }

}
