package testing.tests;

import java.util.Map;

import lib.analyzing.TfIdfCalc;
import lib.article.ArticlesBook;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class AuthorsSharedSubjectsTest extends Test {

    private ArticlesBook allArticles;
    private AuthorsBook authorsInfo;
    private TfIdfCalc tfIdfCalc;


    public AuthorsSharedSubjectsTest() {
    }


    @Override
    public void runTest() {
        System.out.println("Starting Test!" + authorsInfo);

        Map<String, Map<ArticleAuthor, Integer>> sortedSubjs = authorsInfo.sortAuthorsBySubject();

        for (String subj : sortedSubjs.keySet()) {
            Map<ArticleAuthor, Integer> sharingSubjAuthors = sortedSubjs.get(subj);

            System.out.println("users share subj " + subj + " amount: " + sharingSubjAuthors.size());

            for (ArticleAuthor author : sharingSubjAuthors.keySet()) {
                Integer articlesWithSubject = sharingSubjAuthors.get(author);
                System.out.println("author: " + author.getName() + " has " + articlesWithSubject + " out of "
                        + author.getAllArticles().size() + " articles");
            }
            System.out.println();
        }

        // for (ArticleAuthor author : authorsInfo.getAllAuthors().values()) {
        // if (author.getAllArticles().size() < 10) {
        // continue;
        // }
        //
        //// TokensManager authorSubjectsTM = author.getSubjectsTM();
        //// Set<String> allAuthorSubjects = authorSubjectsTM.getAllTokens().keySet();
        // Set<String> allAuthorSubjects = author.getAuthorSubjects();
        // System.out.println("Author " + author.getName());
        // System.out.println("subjects: ");
        // System.out.println(allAuthorSubjects);
        // System.out.println();
        // }

    }


    public ArticlesBook getAllArticles() {
        return allArticles;
    }


    public void setAllArticles(ArticlesBook allArticles) {
        this.allArticles = allArticles;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }

}
