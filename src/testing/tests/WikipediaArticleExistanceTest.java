package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import lib.html_handle.wiki.WikipediaHandler;
import testing.Test;

public class WikipediaArticleExistanceTest extends Test {

    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        @SuppressWarnings("resource")
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("enter expression in hebrew to know if it has its own article in wikipedia database");
            String expression = in.nextLine();
            boolean isExpressionExists = WikipediaHandler.isExpressionKnown(expression);
            System.out.println("is expression exists? -> " + isExpressionExists);
        }
    }

}
