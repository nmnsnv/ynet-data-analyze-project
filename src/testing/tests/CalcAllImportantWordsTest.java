package testing.tests;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lib.MyLib;
import lib.Pair;
import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

/*
 * NOTE: this is more like a system test-like, and it shows the system abillity
 * to calc all words in all documents tf-idf for all authors
 * it also shows the top 10 most important words for each author
 */
public class CalcAllImportantWordsTest extends Test {

    private AuthorsBook authorsInfo;
    private TfIdfCalc tfIdfCalc;
    private boolean dumpMemoryFlag;
    private String dumpPath;


    public CalcAllImportantWordsTest(AuthorsBook authorsInfo, TfIdfCalc tfIdfCalc, String dumpPath,
            boolean dumpMemory) {
        this.authorsInfo = authorsInfo;
        this.tfIdfCalc = tfIdfCalc;
        this.dumpPath = dumpPath;
        this.dumpMemoryFlag = dumpMemory;
    }


    public CalcAllImportantWordsTest() {
    }


    @Override
    public void runTest() throws IOException {
        Map<String, ArticleAuthor> allAuthors = authorsInfo.getAllAuthors();
        int authorsCount = 0;
        for (String authorName : allAuthors.keySet()) {

            // if (authorsCount % 100 == 0 && dumpMemoryFlag) {
            // System.out.println("\n\n___DUMPING CACHE TO DISK___\n\n");
            // tfIdfCalc.dumpTfIdfCalcCache(dumpPath);
            // }

            ArticleAuthor authorObj = allAuthors.get(authorName);
            if (!(authorObj.getName().equals(authorName))) {
                System.out.println("ERROR! authorName " + authorName + " not equals to authorObj.getName(): "
                        + authorObj.getName());
                System.exit(1);
            }
            int numberOfArticles = authorObj.getAllArticles().size();
            // if (numberOfArticles < 10) {
            // continue;
            // }
            authorsCount++;

            if (authorsCount % 100 == 0) {
                System.out.println("ac: " + authorsCount);
            }

            Article itemFromSet = MyLib.getItemFromSet(authorObj.getAllArticles());
            List<Pair<String, Double>> importantWords = authorObj.getImportantWords(tfIdfCalc);

            // System.out.println("Author: |" + authorObj.getName() + "|");
            // System.out.println("number: " + authorsCount);
            // System.out.println("example article id: " + itemFromSet.getId());
            // System.out.println("example article title: " + itemFromSet.getTitle());
            // System.out.println("number of articles: " + numberOfArticles);
            // System.out.println("top 7 important words are: " +
            // getMostImportantWords(importantWords, 7));
            // System.out.println("\n");
        }

        System.out.println("COUNTED " + authorsCount + "authors");

        System.out.println("\n\n");
        System.out.println("Done processing");
        System.out.println("___DUMPING CACHE TO DISK___\n\n");
        if (dumpMemoryFlag) {
            tfIdfCalc.dumpTfIdfCalcCache(dumpPath);
        }
        System.out.println("DONE DUMPING CACHE TO DISK, CACHE PATH: " + dumpPath);
    }


    public List<String> getMostImportantWords(List<Pair<String, Double>> importantWords, int maxLength) {
        List<Pair<String, Double>> firstNElementsList = importantWords.stream().limit(maxLength)
                .collect(Collectors.toList());

        List<String> chosenWords = MyLib.map(firstNElementsList, (pair) -> pair.getObj1());

        return chosenWords;
    }


    public AuthorsBook getAuthorsInfo() {
        return authorsInfo;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public boolean getDumpMemoryFlag() {
        return dumpMemoryFlag;
    }


    public void setDumpMemoryFlag(boolean dumpMemory) {
        this.dumpMemoryFlag = dumpMemory;
    }


    public String getDumpPath() {
        return dumpPath;
    }


    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }

}
