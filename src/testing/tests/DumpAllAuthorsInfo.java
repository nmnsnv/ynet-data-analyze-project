package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.gson.GsonBuilder;

import lib.MyLib;
import lib.article.Article;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class DumpAllAuthorsInfo extends Test {

    private String dumpPath;
    private AuthorsBook authorsInfo;

    class ArticleBasicInfo {
        public String fileName;
        public String title;
    }

    class AuthorInfoDataObj {
        public String authorName;
        public int totalArticlesWritten;
        public Set<String> allSubjectsWritten;
        public List<ArticleBasicInfo> allArticlesBasicInfo = new LinkedList<>();
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        List<AuthorInfoDataObj> allAuthorsData = new LinkedList<>();

        for (ArticleAuthor author : authorsInfo.getAllAuthors().values()) {
            AuthorInfoDataObj aido = new AuthorInfoDataObj();
            allAuthorsData.add(aido);

            aido.authorName = author.getName();
            aido.totalArticlesWritten = author.getAllArticles().size();
            aido.allSubjectsWritten = author.getAuthorSubjects();

            for (Article article : author.getAllArticles()) {
                ArticleBasicInfo abi = new ArticleBasicInfo();
                aido.allArticlesBasicInfo.add(abi);

                abi.fileName = article.getId();
                abi.title = article.getTitle();
            }
        }

        Writer writer = MyLib.openNewFileWriter(dumpPath);
        new GsonBuilder().setPrettyPrinting().create().toJson(allAuthorsData, writer);
    }


    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }

}
