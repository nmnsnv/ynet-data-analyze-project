package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lib.Pair;
import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class DumpSingleAuthorImportantWords extends Test {

    private AuthorsBook authorsInfo;
    private TfIdfCalc tfIdfCalc;
    private String filePath;
    private String authorName;


    public DumpSingleAuthorImportantWords(AuthorsBook authorsInfo, TfIdfCalc tfIdfCalc, String filePath) {
        this.authorsInfo = authorsInfo;
        this.tfIdfCalc = tfIdfCalc;
        this.filePath = filePath;
    }


    public DumpSingleAuthorImportantWords() {
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        ArticleAuthor author = authorsInfo.getAuthor(authorName);
        System.out.println("HERE! author: " + author);
        Map<String, Map<Article, Double>> importantWords = author.getAllWordsTfIdfScore(tfIdfCalc);

        System.out.println("Start searching page");

        // Writer writer = new BufferedWriter(new OutputStreamWriter(new
        // FileOutputStream(filePath), "utf-8"));
        PrintWriter writer = new PrintWriter(filePath, "UTF-8");

        writer.println("author: " + author.getName());
        List<Pair<String, Pair<Article, Double>>> wordsScore = new LinkedList<>();
        for (String word : importantWords.keySet()) {
            Map<Article, Double> wordScores = importantWords.get(word);

            for (Article article : wordScores.keySet()) {
                // writer.println("for word: " + word + " tf-idf = " + score);
                Pair<Article, Double> articleScorePair = new Pair<Article, Double>(article, wordScores.get(article));
                Pair<String, Pair<Article, Double>> scoreObj = new Pair<>(word, articleScorePair);
                wordsScore.add(scoreObj);
            }
        }
        // Map<String, Double> x =
        Collections.sort(wordsScore, new Comparator<Pair<String, Pair<Article, Double>>>() {
            @Override
            public int compare(Pair<String, Pair<Article, Double>> c1, Pair<String, Pair<Article, Double>> c2) {
                // You should ensure that list doesn't contain null values!
                return c1.getObj2().getObj2().compareTo(c2.getObj2().getObj2());
            }
        });

        Collections.reverse(wordsScore);

        for (Pair<String, Pair<Article, Double>> pair : wordsScore) {
            String word = pair.getObj1();
            Article article = pair.getObj2().getObj1();
            String articleName = article.getTitle();
            String articleId = article.getId();
            Double score = pair.getObj2().getObj2();
            writer.println("for word: " + word + " tf-idf = " + score + " || article title: " + articleName
                    + " || article id: " + articleId);
        }

        writer.close();

    }


    public AuthorsBook getAuthorsInfo() {
        return authorsInfo;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public String getFilePath() {
        return filePath;
    }


    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

}
