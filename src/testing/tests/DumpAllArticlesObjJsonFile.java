package testing.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.GsonBuilder;

import lib.Func;
import lib.MyLib;
import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.ArticlesBook;
import testing.Test;

public class DumpAllArticlesObjJsonFile extends Test {

    private ArticlesBook allArticles;
    private TfIdfCalc tfIdfCalc;
    private String dumpPath;

    class FormattedDocument {
        public String fileName;
        public String fileUrl;
        public String title;
        public String author;
        public List<String> subjects;

        public List<String> tags;
        public List<String> mostImportantWordsByOrder;
        public String content;

    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        Writer writer = MyLib.openNewFileWriter(dumpPath);
        Func<FormattedDocument, Article> translateToFormatted = (article) -> {
            FormattedDocument fd = new FormattedDocument();
            fd.content = article.getContent();
            fd.title = article.getTitle();
            fd.fileName = article.getId();
            fd.fileUrl = "www.ynet.co.il/articles/" + article.getId();
            fd.subjects = article.getSubjects();
            fd.tags = article.getTags();
            fd.mostImportantWordsByOrder = new LinkedList<>();
            List<String> docSortedTfIdfScore = null;
            try {
                docSortedTfIdfScore = tfIdfCalc.getDocumentSortedWordsTfIdf(article);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(2);
            }

            List<String> mostImportantItems = new LinkedList<>();
            int maxSize = 15;
            int i = 0;
            for (String word : docSortedTfIdfScore) {
                if (i >= maxSize) {
                    break;
                }
                mostImportantItems.add(word);
                i++;
            }
            fd.mostImportantWordsByOrder = mostImportantItems;
            return fd;
        };
        List<FormattedDocument> allFormattedDocuments = MyLib.map(allArticles.getAllArticles(), translateToFormatted);
        System.out.println("done prepering formatted files, starts dumping");
        new GsonBuilder().setPrettyPrinting().create().toJson(allFormattedDocuments, writer);
        writer.close();
        System.out.println("DONE WRITING TO PATH: " + dumpPath);
    }


    public void setAllArticles(ArticlesBook allArticles) {
        this.allArticles = allArticles;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public void setDumpPath(String dumpPath) {
        this.dumpPath = dumpPath;
    }

}
