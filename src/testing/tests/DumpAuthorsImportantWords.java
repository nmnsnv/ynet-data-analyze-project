package testing.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import lib.Pair;
import lib.analyzing.TfIdfCalc;
import lib.article.author.ArticleAuthor;
import lib.article.author.AuthorsBook;
import testing.Test;

public class DumpAuthorsImportantWords extends Test {
    private AuthorsBook authorsInfo;
    private TfIdfCalc tfIdfCalc;
    private int minRequiredArticles;
    private String dirPath;


    public DumpAuthorsImportantWords(AuthorsBook authorsInfo, TfIdfCalc tfIdfCalc, int minRequiredArticles,
            String dirPath) {
        this.authorsInfo = authorsInfo;
        this.tfIdfCalc = tfIdfCalc;
        this.minRequiredArticles = minRequiredArticles;
        this.dirPath = dirPath;
    }


    @Override
    public void runTest() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        System.out.println("starting test");

        Map<String, ArticleAuthor> allAuthors = authorsInfo.getAllAuthors();
        int authorsCount = 0;
        for (String authorName : allAuthors.keySet()) {

            ArticleAuthor authorObj = allAuthors.get(authorName);
            if (!(authorObj.getName().equals(authorName))) {
                System.out.println("ERROR! authorName " + authorName + " not equals to authorObj.getName(): "
                        + authorObj.getName());
                System.exit(1);
            }
            int numberOfArticles = authorObj.getAllArticles().size();
            if (numberOfArticles < minRequiredArticles) {
                continue;
            }
            authorsCount++;

            if (authorsCount % 100 == 0) {
                System.out.println("authors done dumping count: " + authorsCount);
            }

            List<Pair<String, Double>> importantWords = authorObj.getImportantWords(tfIdfCalc);
            String filePath = dirPath + authorName.replaceAll(" ", "_") + ".txt";
            System.out.println("Path: " + filePath);
            File curFile = new File(filePath);
            curFile.createNewFile();
            PrintWriter writer = new PrintWriter(filePath, "UTF-8");
            for (Pair<String, Double> pair : importantWords) {
                writer.println("Word: " + pair.getObj1() + " tfIdf: " + pair.getObj2());
            }
            writer.close();
        }
        System.out.println("COUNTED " + authorsCount + "authors");
    }


    public DumpAuthorsImportantWords() {
    }


    public AuthorsBook getAuthorsInfo() {
        return authorsInfo;
    }


    public void setAuthorsInfo(AuthorsBook authorsInfo) {
        this.authorsInfo = authorsInfo;
    }


    public TfIdfCalc getTfIdfCalc() {
        return tfIdfCalc;
    }


    public void setTfIdfCalc(TfIdfCalc tfIdfCalc) {
        this.tfIdfCalc = tfIdfCalc;
    }


    public int getMinRequiredArticles() {
        return minRequiredArticles;
    }


    public void setMinRequiredArticles(int minRequiredArticles) {
        this.minRequiredArticles = minRequiredArticles;
    }


    public String getDirPath() {
        return dirPath;
    }


    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }

}
