package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import lib.Func;
import lib.analyzing.TfIdfCalc;
import lib.article.Article;
import lib.article.ArticlesBook;
import lib.article.author.AuthorsBook;
import lib.html_handle.MyHTMLParser;
import testing.tests.AuthorWordImportanceInArticlesTest;
import testing.tests.AuthorsSharedSubjectsTest;
import testing.tests.CalcAllImportantWordsTest;
import testing.tests.DumpAllArticlesObjJsonFile;
import testing.tests.DumpAllAuthorsCommonSubjects;
import testing.tests.DumpAllAuthorsInfo;
import testing.tests.DumpAllAuthorsUnderSameSubject;
import testing.tests.DumpAllTfIdfWordsInArticleTest;
import testing.tests.DumpSingleAuthorImportantWords;
import testing.tests.FindAuthorsForAuthorWithSubj;
import testing.tests.PrintAllAuthorArticlesTest;
import testing.tests.WikipediaArticleExistanceTest;
import testing.tests.DumpAuthorsImportantWords;
import testing.tests.WordScannerTest;

public class Main {

    public static final String TF_IDF_CALC_DEFAULT_PATH = "/home/noam/work/projects/army/ynet/parsed_data/tfIdfCalcCache.cache";

    static DumpSingleAuthorImportantWords dsaiw = new DumpSingleAuthorImportantWords();
    static CalcAllImportantWordsTest caiwt = new CalcAllImportantWordsTest();
    static PrintAllAuthorArticlesTest paaat = new PrintAllAuthorArticlesTest();
    static AuthorsSharedSubjectsTest asst = new AuthorsSharedSubjectsTest();
    static FindAuthorsForAuthorWithSubj fafaws = new FindAuthorsForAuthorWithSubj();
    static WordScannerTest wsc = new WordScannerTest();
    static DumpAllTfIdfWordsInArticleTest dumpWordsInArticle = new DumpAllTfIdfWordsInArticleTest();
    static AuthorWordImportanceInArticlesTest awiiat = new AuthorWordImportanceInArticlesTest();
    static DumpAuthorsImportantWords daiw = new DumpAuthorsImportantWords();
    static WikipediaArticleExistanceTest waet = new WikipediaArticleExistanceTest();
    static DumpAllArticlesObjJsonFile daaojf = new DumpAllArticlesObjJsonFile();
    static DumpAllAuthorsCommonSubjects daacs = new DumpAllAuthorsCommonSubjects();
    static DumpAllAuthorsUnderSameSubject daauss = new DumpAllAuthorsUnderSameSubject();
    static DumpAllAuthorsInfo daai = new DumpAllAuthorsInfo();


    public static void main(String[] args) throws IOException {

        // WikipediaHandler.isExpressionKnown("מחשב");

        String articlesPath = "/home/noam/work/projects/army/ynet/ynet_data/new/www.ynet.co.il/articles/";
        AuthorsBook authorsInfo = new AuthorsBook();
        ArticlesBook allArticles = new ArticlesBook();
        Func<Article, File> articleLoader = getArticleLoader();

        loadAllArticles(articlesPath, authorsInfo, allArticles, articleLoader);

        boolean useWikiFlag = false;
        TfIdfCalc tfIdfCalc = new TfIdfCalc(allArticles, useWikiFlag);
        tfIdfCalc.setWikiKnownCoefficient(4d);

        // loadTfIdfCalcCache(tfIdfCalc);

        setupTests(authorsInfo, allArticles, tfIdfCalc);

        //////////////////////////////////////
        /////////// TESTING SECTION //////////
        //////////////////////////////////////

        long nanoTimeStart = System.nanoTime();

        daai.runTest();

        // daauss.runTest();

        // daacs.runTest();

        // daaojf.runTest();

        /*
         * NOTE: choose the test we want to run, notice only 1 should be running at a
         * time since some scripts enters an infinite loop for taking input from the
         * user, so it wont run the rest, furthermore, there will be many outputs in the
         * console which will erase the oldest and thus making some tests entirely
         * invisible
         */

        /*
         * a wikipedia search example, it tells whether or not an article exists in
         * their databases
         */
        // waet.runTest();

        /*
         * dumps all important words of a single author into a file with each word
         * tf-idf score
         */
        // dsaiw.runTest();

        /*
         * creates file for each author with more than a minimum articles written
         * containing all the important words with their tf-idf score in order from the
         * highest to lowest.
         * 
         * very simmilar to dsaiw instance but here its for many authors
         */
        // daiw.runTest();

        /*
         * this is more like a system test-like, and it shows the system abillity to
         * calc all words in all documents tf-idf for all authors it also shows the top
         * 10 most important words for each author its output is canceled since its
         * slows down overall performance a lot for accessing the console many times, it
         * also creates a json file in the wanted path containing all important words in
         * all documents with their scores
         */
        // caiwt.runTest();

        /*
         * prints all the articles of a specific author with their subjects accordingly
         */
        // paaat.runTest();

        /*
         * prints all the subjects and for each subject it prints the authors who has
         * written at least one article in that topic and also the amount of articles
         * the author has written in the specific subject
         */
        // asst.runTest();

        /*
         * an authors name and try to find colleagues of the author by sharing the same
         * subjects, this is good for trying to identify people who the person might now
         * since they have same habits
         * 
         * TODO make the connections stronger by sharing more common subjects this takes
         */
        // fafaws.runTest();

        /*
         * recieves an input from the user for specific word and prints all the
         * documents which the word has above defined minimum tf-idf score with
         * information of the document and about the word in the document such as its
         * specific tf-idf score, tf, and idf
         */
        // wsc.runTest();

        /*
         * searches for word in specific author and prints all the fitting info of the
         * word and the authors articles, good for finding wether the author has
         * relations with some terms such as specific types of rockets, planes, cars
         * etc.
         */
        // awiiat.runTest();

        /*
         * dumps all the words in an article as well as their tf-idf scores
         */
        // dumpWordsInArticle.runTest();

        ///////////////////////////////////////
        ///////// END TESTING SECTION /////////
        ///////////////////////////////////////
        long nanoTimeEnd = System.nanoTime();
        long nanoTimePassed = nanoTimeEnd - nanoTimeStart;
        double secTimePassed = nanoTimePassed / 1000000000d;
        System.out.println("test time: " + secTimePassed);

        // tfIdfCalc.dumpTfIdfCalcCache(TF_IDF_CALC_DEFAULT_PATH);
        System.out.println("___DONE___");
    }


    public static void setupTests(AuthorsBook authorsInfo, ArticlesBook allArticles, TfIdfCalc tfIdfCalc)
            throws FileNotFoundException, UnsupportedEncodingException, IOException {

        // DumpAllAuthorsInfo
        daai.setDumpPath("/home/noam/work/projects/army/ynet/toSendJsonData/allAuthorsInfo.json");
        daai.setAuthorsInfo(authorsInfo);

        // DumpAllAuthorsUnderSameSubject
        daauss.setDumpPath("/home/noam/work/projects/army/ynet/toSendJsonData/allAuthorsUnderSameSubject.json");
        daauss.setAuthorsInfo(authorsInfo);

        // DumpAllAuthorsCommonSubjects
        daacs.setDumpPath("/home/noam/work/projects/army/ynet/toSendJsonData/allAuthorsCommonSubjects.json");
        daacs.setAuthorsInfo(authorsInfo);

        // DumpAllArticlesObjJsonFile
        daaojf.setDumpPath("/home/noam/work/projects/army/ynet/toSendJsonData/allArticlesInfo.json");
        daaojf.setAllArticles(allArticles);
        daaojf.setTfIdfCalc(tfIdfCalc);

        // PrintAllAuthorArticlesTest
        paaat.setAuthor("הדי בירן");
        paaat.setAuthorsInfo(authorsInfo);
        paaat.setTfIdfCalc(tfIdfCalc);

        // AuthorsSharedSubjectsTest
        asst.setAllArticles(allArticles);
        asst.setTfIdfCalc(tfIdfCalc);
        asst.setAuthorsInfo(authorsInfo);

        // FindAuthorsForAuthorWithSubj
        fafaws.setAuthorName("הדי בירן");
        fafaws.setAllArticles(allArticles);
        fafaws.setTfIdfCalc(tfIdfCalc);
        fafaws.setAuthorsInfo(authorsInfo);

        // WordScannerTest
        wsc.setAllArticles(allArticles);
        wsc.setTfIdfCalc(tfIdfCalc);
        wsc.setMinTfIdf(1d);

        // DumpAllTfIdfWordsInArticleTest
        dumpWordsInArticle.setArticle(allArticles.getArticleByIndex(0));
        dumpWordsInArticle.setTfIdfCalc(tfIdfCalc);
        dumpWordsInArticle.setDumpPath("/home/noam/work/projects/army/ynet/research/tfidf_test.txt");

        // CalcAllImportantWordsTest
        caiwt.setAuthorsInfo(authorsInfo);
        caiwt.setTfIdfCalc(tfIdfCalc);
        caiwt.setDumpPath(TF_IDF_CALC_DEFAULT_PATH);
        caiwt.setDumpMemoryFlag(false);

        // DumpSingleAuthorImportantWords
        dsaiw.setAuthorsInfo(authorsInfo);
        dsaiw.setAuthorName("אודי עציון");
        dsaiw.setTfIdfCalc(tfIdfCalc);
        dsaiw.setFilePath("/home/noam/work/projects/army/ynet/research/author_scores_test.txt");

        // AuthorWordImportanceInArticlesTest
        awiiat.setAuthorsInfo(authorsInfo);
        awiiat.setTfIdfCalc(tfIdfCalc);
        awiiat.setAuthorName("אודי עציון");

        // DumpAuthorsImportantWords
        daiw.setAuthorsInfo(authorsInfo);
        daiw.setTfIdfCalc(tfIdfCalc);
        daiw.setMinRequiredArticles(10);
        // NOTE: the path should be created already when executed
        daiw.setDirPath("/home/noam/work/projects/army/ynet/research/authors_important_words/no_wiki/");
    }


    public static void loadAllArticles(String articlesPath, AuthorsBook authorsInfo, ArticlesBook allArticles,
            Func<Article, File> articleLoader) throws IOException {
        System.out.println("loading all articles");
        allArticles.loadArticlesInDir(articlesPath, authorsInfo, articleLoader);
        System.out.println("done loading all articles");
    }


    public static Func<Article, File> getArticleLoader() {
        return (file) -> {
            MyHTMLParser myParser = new MyHTMLParser(file);
            Article curArticle = null;
            try {
                curArticle = myParser.parse();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return curArticle;
        };
    }


    public static void loadTfIdfCalcCache(TfIdfCalc tfIdfCalc) throws IOException {
        System.out.println("start loading cache");
        tfIdfCalc.loadTfIdfCalcCache(TF_IDF_CALC_DEFAULT_PATH);
        System.out.println("done loading cache");
    }

}
